import Head from 'next/head';
import Image from 'next/image';
import styles from '../styles/success.module.css';
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import Loader from "../components/loader/loader";

export default function Home() {
  const router = useRouter();
 


  return (
    <div className={styles.formCard}>
      <h1 className={styles.success}>
       Your broker will be in touch
      </h1>
      <p className={styles.successP}>Please verify your account by clicking on the link sent to your email.</p>

      <button className={styles.button} onClick={() => {window.location.href="https://www.folio.insure"}}>Sign in to Folio</button>
     
    </div>
  )
}
